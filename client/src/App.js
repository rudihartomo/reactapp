import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Web3 from 'web3';
import './App.css';
import Marketplace from './contracts/Marketplace.json';
import Navbar from './components/Navbar';
import Main from './components/Main';
import Buy from './components/Buy';
import Sell from './components/Sell';
import Create from './components/Create';

const fs = require('fs');
const axios = require('axios');

const FormData = require('form-data');

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account: '',
            productCount: 0,
            products: [],
            currentProduct: null,
            loading: true,
            //ipfsHash: '',
            ipfsHash: 'QmZea8jgGgSuwog2BkSuStMaggruact7iA7Do6G6qUz5cs',
            web3: null,
            buffer: null,
            limit: 2,
            page: 1,
        };

        this.fileInput = React.createRef();

        this.createProduct = this.createProduct.bind(this);
        this.purchaseProduct = this.purchaseProduct.bind(this);
        this.createMultipleProduct = this.createMultipleProduct.bind(this);
        this.getDetailProduct = this.getDetailProduct.bind(this);

        this.captureFile = this.captureFile.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    async componentWillMount() {
        await this.loadWeb3();
        await this.loadBlockchainData();
    }

    async loadWeb3() {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            await window.ethereum.enable();
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider);
        } else {
            window.alert(
                'Non-Ethereum browser detected. You should consider trying MetaMask!',
            );
        }
    }

    async loadBlockchainData() {
        const web3 = window.web3;
        // Load account
        const accounts = await web3.eth.getAccounts();
        this.setState({account: accounts[0]});
        let balance = await web3.eth.getBalance(accounts[0]);
        await this.setState({balance: web3.utils.fromWei(balance, 'Ether')});
        const networkId = await web3.eth.net.getId();
        const networkData = Marketplace.networks[networkId];
        if (networkData) {
            const marketplace = new web3.eth.Contract(
                Marketplace.abi,
                networkData.address,
                //'0xF68a6D04F02897d38F8D0Bb6e107F3fB0404f585',
            );
            this.setState({marketplace});
            const productCount = await marketplace.methods
                .productCount()
                .call();
            this.setState({productCount});
            const offset = (this.state.page - 1) * this.state.limit + 1;
            const endItem =
                offset * this.state.limit < productCount
                    ? offset * this.state.limit
                    : productCount;
            console.log(offset);
            // Load products
            for (var i = offset; i <= endItem; i++) {
                const product = await marketplace.methods.products(i).call();
                this.setState({
                    products: [...this.state.products, product],
                });
            }
            this.setState({loading: false});
        } else {
            window.alert(
                'Marketplace contract not deployed to detected network.',
            );
        }
    }

    async getDetailProduct(i) {
        const web3 = window.web3;
        const networkId = await web3.eth.net.getId();
        const networkData = Marketplace.networks[networkId];
        if (networkData) {
            const marketplace = new web3.eth.Contract(
                Marketplace.abi,
                networkData.address,
                //'0xF68a6D04F02897d38F8D0Bb6e107F3fB0404f585',
            );
            // Load product
            const product = await marketplace.methods.products(i).call();
            console.log(product);
            const currentProduct = {
                name: product.name,
                price: product.price,
                metadata: JSON.parse(product.metadata),
            };

            this.setState({currentProduct: currentProduct});
        }
    }

    createProduct(name, price) {
        this.setState({loading: true});
        this.state.marketplace.methods
            .createProduct(name, price)
            .send({from: this.state.account})
            .once('receipt', async (receipt) => {
                this.setState({
                    products: [
                        ...this.state.products,
                        receipt.events.ProductCreated.returnValues,
                    ],
                });
                window.location.reload();
            });
    }

    async createMultipleProduct(
        qty,
        name,
        price,
        duration,
        description,
        thumbnail,
        tags,
        productionYear,
        dimention,
    ) {
        for (var i = 0; i < qty; i++) {
            await this.state.marketplace.methods
                .createProduct(
                    name,
                    price,
                    JSON.stringify({
                        hasfile: this.state.ipfsHash,
                        duration: duration,
                        description: description,
                        thumbnail: thumbnail,
                        tags: tags,
                        productionYear: productionYear,
                        dimention: dimention,
                    }),
                )
                .send({from: this.state.account})
                .once('receipt', async (receipt) => {
                    console.log(receipt);
                    //   this.setState({
                    //     products: [
                    //       ...this.state.products,
                    //       receipt.events.ProductCreated.returnValues,
                    //     ],
                    //   });
                });
        }
    }

    purchaseProduct(id, price) {
        this.setState({loading: true});
        this.state.marketplace.methods
            .purchaseProduct(id)
            .send({from: this.state.account, value: price})
            .once('receipt', async (receipt) => {
                console.log(receipt);
                window.location.reload();
            })
            .catch((e) => {
                // Transaction rejected or failed
                console.log(e);
            });
    }

    async captureFile(event) {
        event.preventDefault();
        const file = event.target.files[0];

        const form_data = new FormData();
        try {
            form_data.append('file', file);
            const request = {
                method: 'post',
                url: 'https://api.pinata.cloud/pinning/pinFileToIPFS',
                maxContentLength: 'Infinity',
                headers: {
                    pinata_api_key: 'f760c4dec9a2e0524697',
                    pinata_secret_api_key:
                        'b0d45ddab97d474939d4896af9a95da3456ee8fcf3f63b1ff31207eceb47fc7a',
                    'Content-Type': `multipart/form-data; boundary=${form_data._boundary}`,
                },
                data: form_data,
            };
            console.log('request:', request);
            const response = await axios(request);
            console.log('Successfully pinned file to IPFS : ', response);
            this.setState({ipfsHash: response.data.IpfsHash, loading: false});
        } catch (err) {
            console.log('Error occurred while pinning file to IPFS: ', err);
        }
        // const reader = new window.FileReader();
        // reader.readAsArrayBuffer(file);

        // reader.onloadend = () => {
        //     this.setState({buffer: Buffer(reader.result)});
        // };
    }

    async pinFileToIPFSfromPath(filePath) {
        const pinataEndpoint = 'https://api.pinata.cloud/pinning/pinFileToIPFS';
        const pinataApiKey = 'f760c4dec9a2e0524697';
        const pinataApiSecret =
            'b0d45ddab97d474939d4896af9a95da3456ee8fcf3f63b1ff31207eceb47fc7a';
        const form_data = new FormData();
        try {
            form_data.append('file', fs.createReadStream(filePath));
            const request = {
                method: 'post',
                url: pinataEndpoint,
                maxContentLength: 'Infinity',
                headers: {
                    pinata_api_key: pinataApiKey,
                    pinata_secret_api_key: pinataApiSecret,
                    'Content-Type': `multipart/form-data; boundary=${form_data._boundary}`,
                },
                data: form_data,
            };
            console.log('request:', request);
            const response = await axios(request);
            console.log('Successfully pinned file to IPFS : ', response);
            // await storeDataToFile(response.data);
            // console.log('Successfully added IPFS response to json file');
        } catch (err) {
            console.log('Error occurred while pinning file to IPFS: ', err);
        }
    }

    async onSubmit(event) {
        event.preventDefault();
        this.setState({loading: true});

        // ipfs.files.add(this.state.buffer, (error, result) => {
        //     if (error) {
        //         console.error(error);
        //         return;
        //     }
        //     console.log(result);
        //     this.setState({ipfsHash: result[0].hash, loading: false});
        //     //   this.simpleStorageInstance
        //     //     .set(result[0].hash, { from: this.state.account })
        //     //     .then((r) => {
        //     //       return this.setState({ ipfsHash: result[0].hash });
        //     //       console.log("ifpsHash", this.state.ipfsHash);
        //     //     });
        // });
    }

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route
                        exact
                        path="/"
                        render={(props) => {
                            return (
                                <div>
                                    <Navbar
                                        account={this.state.account}
                                        balance={this.state.balance}
                                        active="home"
                                    />
                                    <div
                                        className="container"
                                        style={{marginTop: '50px'}}>
                                        <div className="row">
                                            <main
                                                role="main"
                                                className="col-lg-12 d-flex">
                                                {this.state.loading ? (
                                                    <div
                                                        id="loader"
                                                        className="text-center">
                                                        <p className="text-center">
                                                            Loading...
                                                        </p>
                                                    </div>
                                                ) : (
                                                    <Main />
                                                )}
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            );
                        }}
                    />
                    <Route
                        exact
                        path="/upload"
                        render={(props) => {
                            return (
                                <div>
                                    <Navbar
                                        account={this.state.account}
                                        balance={this.state.balance}
                                        active="upload"
                                    />
                                    <div
                                        className="container"
                                        style={{marginTop: '50px'}}>
                                        <div className="row">
                                            <main
                                                role="main"
                                                className="col-lg-12 d-flex">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <h1>Your Instrument</h1>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <p>
                                                            This file is stored
                                                            on IPFS & The
                                                            Ethereum Blockchain!
                                                        </p>
                                                    </div>
                                                    <div className="col-md-12">
                                                        {this.state.ipfsHash !==
                                                        '' ? (
                                                            <div>
                                                                <audio controls>
                                                                    <source
                                                                        src={`https://ipfs.io/ipfs/${this.state.ipfsHash}`}
                                                                        type="audio/mpeg"
                                                                    />
                                                                    Your browser
                                                                    does not
                                                                    support the
                                                                    audio
                                                                    element.
                                                                </audio>
                                                                <Create
                                                                    account={
                                                                        this
                                                                            .state
                                                                            .account
                                                                    }
                                                                    createMultipleProduct={
                                                                        this
                                                                            .createMultipleProduct
                                                                    }
                                                                />
                                                            </div>
                                                        ) : null}
                                                    </div>
                                                    {this.state.loading ? (
                                                        <div
                                                            id="loader"
                                                            className="col-md-12">
                                                            <p className="text-center">
                                                                Loading...
                                                            </p>
                                                        </div>
                                                    ) : null}
                                                    {!this.state.loading &&
                                                    this.state.ipfsHash ===
                                                        '' ? (
                                                        <div className="col-md-12">
                                                            <h2>Upload File</h2>
                                                            <form
                                                                onSubmit={
                                                                    this
                                                                        .onSubmit
                                                                }>
                                                                <input
                                                                    type="file"
                                                                    onChange={
                                                                        this
                                                                            .captureFile
                                                                    }
                                                                />
                                                                <button
                                                                    type="submit"
                                                                    className="btn btn-info">
                                                                    Upload
                                                                </button>
                                                            </form>
                                                        </div>
                                                    ) : null}
                                                </div>
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            );
                        }}
                    />

                    <Route
                        exact
                        path="/sell"
                        render={(props) => {
                            return (
                                <div>
                                    <Navbar
                                        account={this.state.account}
                                        balance={this.state.balance}
                                        active="sell"
                                    />
                                    <div
                                        className="container"
                                        style={{marginTop: '50px'}}>
                                        <div className="row">
                                            <main
                                                role="main"
                                                className="col-lg-12 d-flex">
                                                {this.state.loading ? (
                                                    <div
                                                        id="loader"
                                                        className="text-center">
                                                        <p className="text-center">
                                                            Loading...
                                                        </p>
                                                    </div>
                                                ) : (
                                                    <Sell
                                                        products={
                                                            this.state.products
                                                        }
                                                        account={
                                                            this.state.account
                                                        }
                                                        createProduct={
                                                            this.createProduct
                                                        }
                                                        getDetailProduct={
                                                            this
                                                                .getDetailProduct
                                                        }
                                                    />
                                                )}
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            );
                        }}
                    />

                    <Route
                        exact
                        path="/buy"
                        render={(props) => {
                            return (
                                <div>
                                    <Navbar
                                        account={this.state.account}
                                        balance={this.state.balance}
                                        active="buy"
                                    />
                                    <div
                                        className="container"
                                        style={{marginTop: '50px'}}>
                                        <div className="row">
                                            {this.state.loading ? (
                                                <div
                                                    id="loader"
                                                    className="text-center">
                                                    <p className="text-center">
                                                        Loading...
                                                    </p>
                                                </div>
                                            ) : (
                                                <Buy
                                                    account={this.state.account}
                                                    products={
                                                        this.state.products
                                                    }
                                                    purchaseProduct={
                                                        this.purchaseProduct
                                                    }
                                                />
                                            )}
                                        </div>
                                    </div>
                                </div>
                            );
                        }}
                    />

                    <Route
                        exact
                        path="/detail/:id"
                        render={(props) => {
                            console.log(props.match.params.id);
                            this.getDetailProduct(props.match.params.id);
                            return (
                                <div>
                                    <Navbar
                                        account={this.state.account}
                                        balance={this.state.balance}
                                        active="buy"
                                    />
                                    {this.state.currentProduct != null ? (
                                        <div
                                            className="container"
                                            style={{marginTop: '50px'}}>
                                            <div className="row">
                                                <div className="col-md-2">
                                                    Nama
                                                </div>
                                                <div className="col-md-10">
                                                    :{' '}
                                                    {
                                                        this.state
                                                            .currentProduct.name
                                                    }
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-2">
                                                    Duration
                                                </div>
                                                <div className="col-md-10">
                                                    :{' '}
                                                    {
                                                        this.state
                                                            .currentProduct
                                                            .metadata.duration
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    ) : null}
                                </div>
                            );
                        }}
                    />
                    <Route
                        exact
                        path="/paging"
                        render={(props) => {
                            this.getPagingData(2, 1);
                            return (
                                <div>
                                    <Navbar
                                        account={this.state.account}
                                        balance={this.state.balance}
                                        active="buy"
                                    />
                                    {this.state.currentProduct != null ? (
                                        <div
                                            className="container"
                                            style={{marginTop: '50px'}}>
                                            <div className="row">
                                                <div className="col-md-2">
                                                    Nama
                                                </div>
                                                <div className="col-md-10">
                                                    :{' '}
                                                    {
                                                        this.state
                                                            .currentProduct.name
                                                    }
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-2">
                                                    Duration
                                                </div>
                                                <div className="col-md-10">
                                                    :{' '}
                                                    {
                                                        this.state
                                                            .currentProduct
                                                            .metadata.duration
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    ) : null}
                                </div>
                            );
                        }}
                    />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
