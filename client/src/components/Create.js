import React, {Component} from 'react';

export class Create extends Component {
    render() {
        return (
            <div className="col-md-12">
                <div className="col-md-4">
                    <h1>Create Product</h1>
                    <form
                        onSubmit={async (event) => {
                            event.preventDefault();
                            const web3 = window.web3;
                            //const address = await web3.eth.getAccounts();

                            const qty = this.productQty.value;
                            const name = this.productName.value;
                            const price = window.web3.utils.toWei(
                                this.productPrice.value.toString(),
                                'Ether',
                            );
                            const duration = this.productDuration.value;
                            const description = this.productDescription.value;
                            const thumbnail = this.productThumbnail.value;
                            const tags = this.productTags.value;
                            const productionYear =
                                this.productProductionYear.value;
                            const dimention = this.productDimention.value;
                            this.props.createMultipleProduct(
                                qty,
                                name,
                                price,
                                duration,
                                description,
                                thumbnail,
                                tags,
                                productionYear,
                                dimention,
                            );
                        }}>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productPrice">Qty</label>
                            <input
                                id="productQty"
                                type="text"
                                ref={(input) => {
                                    this.productQty = input;
                                }}
                                className="form-control"
                                placeholder="1"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productName">Product Name</label>
                            <input
                                id="productName"
                                type="text"
                                ref={(input) => {
                                    this.productName = input;
                                }}
                                className="form-control"
                                placeholder="Morning Instrument"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productPrice">
                                Product Price (ETH)
                            </label>
                            <input
                                id="productPrice"
                                type="text"
                                ref={(input) => {
                                    this.productPrice = input;
                                }}
                                className="form-control"
                                placeholder="1.2"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productDescription">
                                Description
                            </label>
                            <input
                                id="productDescription"
                                type="text"
                                ref={(input) => {
                                    this.productDescription = input;
                                }}
                                className="form-control"
                                placeholder="Descriptoin"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productThumbnail">Thumbnail</label>
                            <input
                                id="productThumbnail"
                                type="text"
                                ref={(input) => {
                                    this.productThumbnail = input;
                                }}
                                className="form-control"
                                placeholder="Thumbnail URL"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productPrice">
                                Duration (seconds)
                            </label>
                            <input
                                id="productDuration"
                                type="text"
                                ref={(input) => {
                                    this.productDuration = input;
                                }}
                                className="form-control"
                                placeholder="300"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productTags">Tags</label>
                            <input
                                id="productTags"
                                type="text"
                                ref={(input) => {
                                    this.productTags = input;
                                }}
                                className="form-control"
                                placeholder="Tags"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productProductionYear">
                                Production Year
                            </label>
                            <input
                                id="productTags"
                                type="text"
                                ref={(input) => {
                                    this.productProductionYear = input;
                                }}
                                className="form-control"
                                placeholder="Production Year"
                                required
                            />
                        </div>
                        <div className="form-group mr-sm-2">
                            <label htmlFor="productDimention">
                                Product Dimention
                            </label>
                            <input
                                id="productTags"
                                type="text"
                                ref={(input) => {
                                    this.productDimention = input;
                                }}
                                className="form-control"
                                placeholder="Product Dimention"
                                required
                            />
                        </div>
                        <button type="submit" className="btn btn-info">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Create;
