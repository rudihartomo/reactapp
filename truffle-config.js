const path = require('path');
const HDWalletProvider = require('@truffle/hdwallet-provider');

const mnemonic =
    'ankle tent catalog oval pulse vintage victory art exist unhappy pool false';
const clientURL =
    'https://rinkeby.infura.io/v3/e3f1e74e59be4ea681117dec0eff6bfb';

module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    contracts_build_directory: path.join(__dirname, 'client/src/contracts'),
    networks: {
        develop: {
            port: 8545,
        },
        rinkeby: {
            provider: () => new HDWalletProvider(mnemonic, clientURL),
            network_id: 4, // Rinkeby's id
            gas: 3000000,
            confirmations: 2, // # of confs to wait between deployments. (default: 0)
            timeoutBlocks: 200, // # of blocks before a deployment times out  (minimum/default: 50)
            skipDryRun: true, // Skip dry run before migrations? (default: false for public nets )
            networkCheckTimeout: 20000000,
            //from: '0xFDdde5cAe084556C09914e2a5D14A84EBc52ED1A',
        },
    },
};
